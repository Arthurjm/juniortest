<?php

namespace App\Repositories;

use App\Models\Product;
use Src\Database\DB;

class ProductRepository extends AbstractProductRepository
{
    /**
     * Model that maps the product table
     *
     * @var [type]
     */
    private $productModel; 

    /**
     * Inject the product's model dependency
     *
     * @param Product $productModel
     */
    public function __construct(Product $productModel)
    {
        $this->productModel = $productModel;
    }

    /**
     * Retrieve a registry by it's id
     *
     * @param integer $id
     * @return Product
     */
    public function getById(int $id)
    {
        return $this->productModel->find($id);
    }

    /**
     * Retrieve all product records from the database
     *
     * @return void
     */
    public function getAllProducts()
    {
        $products = $this->productModel->all();
        
        //Load the child table properties on each product
        foreach ($products as $product) {            
            $product->type();
        }
        return $products;
    }

    /**
     * Delete a product from the database by it's id
     *
     * @param integer $id
     * @return void
     */
    public function deleteProduct(int $id)
    {
        $this->productModel->find($id);
        $this->productModel->destroy();
        
        //Delete the related record on the child table
        DB::delete($this->productModel->type_id, $this->productModel->type);
    }


    /**
     * Insert a product in the database
     *
     * @param array $fields
     * @return bool
     */
    public function insertProduct(array $fields)
    {
        //Filter the fields so it sends the proper fields to each table 
        $fieldsProducts = $fields;
        unset($fields['sku'], $fields['name'], $fields['price'], $fields['type']);

        //Insert the the proper fields in the table described by $fieldProducts['type']
        $stmt = DB::insert($fields, $fieldsProducts['type']);

        //Tests if the inserction was a failure
        if (!$stmt['status']) {
            return false;
        }

        //Insert the product in the products's table
        $stmt = $this->productModel->insert([
            'sku' => $fieldsProducts['sku'],
            'name' => $fieldsProducts['name'],
            'price' => $fieldsProducts['price'],
            'type' => $fieldsProducts['type'],
            'type_id' => DB::getLastIsertedId()
        ]);

        //Tests if the second inserction was a failure
        if (!$stmt['status']) {
            //In case it fails, deletes the previously inserted registry
            DB::delete(DB::getLastIsertedId(), $fieldsProducts['type']);
            return false;
        }
        return true;
    }
}
