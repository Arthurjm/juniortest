<?php

namespace App\Repositories;

abstract class AbstractProductRepository
{
  //Abstract class in case a new repository method is needed and it could be reused by all products repositories
}