<?php

namespace App\Controllers;

use App\Repositories\ProductRepository;
use Rakit\Validation\Validator;
use Src\Controllers\Controller;
use Src\Validator\UniqueRule;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    /**
     * Product repository
     *
     * @var ProductRepository
     */
    private $repository;

    /**
     * Request validator
     *
     * @var Validator
     */
    private $validator;

    /**
     * Inject the repository and the validator
     *
     * @param ProductRepository $repository
     * @param Validator $validator
     */
    public function __construct(ProductRepository $repository, Validator $validator)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    /**
     * Retrieve all products from the database
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getProducts(Request $request, Response $response)
    {
        $data = $this->repository->getAllProducts();        
        //Tests if the products could be retrieved
        if ($data == null) {
            $result = json_encode(['status' => '500', 'message' => 'An  error has occurred when trying to get all the products']);
            $response->setContent($result)->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $data = json_encode($data);
            $response->setContent($data);
        }
        return $response->send();
    }

    /**
     * Create new product in the database
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function postProduct(Request $request, Response $response)
    {     
        $this->validator->addValidator('unique', new UniqueRule());
        $validation = $this->validator->validate($request->toArray(), [
            'sku' => 'required|unique:products,sku',
            'name' => 'required',
            'price' => 'required|numeric',
            'type' => "required|in:dvd_discs,books,furnitures",
            'size' => 'required_if:type,dvd_discs',
            'weight' => 'required_if:type,books',
            'height' => 'required_if:type,furnitures',
            'width' => 'required_if:type,furnitures',
            'length' => 'required_if:type,furnitures'
        ]);

        $validation->validate();

        //Tests if the validation failed
        if ($validation->fails()) {            
            $errors = $validation->errors();
            $errors = ['status' => '422', 'message' => $errors->firstOfAll()];            
            $response->setContent(json_encode($errors))->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response->send();            
        } 

        //Tests if the product could be inserted
        //Returns status 500 in case of any problem related to the database
        if (!$this->repository->insertProduct($request->toArray())) {
            $result = json_encode(['status' => '500', 'message' => 'An error has occurred when trying to create the product']);
            $response->setContent($result)
                ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);                
        } else {
            $result = json_encode(['status' => '201', 'message' => 'Product successfully created']);
            $response->setContent($result)
                ->setStatusCode(Response::HTTP_CREATED);                
        }
        return $response->send();
    }

    /**
     * Delete a product on the database
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function deleteProduct(Request $request, Response $response)
    {
        $product = $this->repository->getById($request->query->get('id'));

        //Tests if the product could be found
        if (!$product) {
            $response->setContent(json_encode(['status' => 404, 'message' => 'Error! Could not find the required resource']))
                ->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response->send();
        }

        $this->repository->deleteProduct($request->query->get('id'));

        $response->setStatusCode(Response::HTTP_OK);

        return $response->send();
    }
}
