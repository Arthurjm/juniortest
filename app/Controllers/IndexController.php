<?php

namespace App\Controllers;

use Src\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource
     *
     * @return void
     */
    public function index(Request $request, Response $response)
    {
        return $this->view('index/index');  
    }

    /**
     * Display the form to create new resources
     *
     * @return void
     */
    public function create()
    {
        return $this->view('index/create');
    }
}
