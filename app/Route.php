<?php

namespace App;

use Src\Routes\Bootstrap;

class Route extends Bootstrap
{
    /**
     * List of routes
     *
     * @var array
     */
    private $routes = [
        'getProducts' => [
            'method' => 'GET',
            'route' => '/product',
            'controller' => 'ProductController',
            'action' => 'getProducts'
        ],
        'postProducts' => [
            'method' => 'POST',
            'route' => '/product/add',
            'controller' => 'ProductController',
            'action' => 'postProduct'
        ],
        'deleteProducts' => [
            'method' => 'GET',
            'route' => '/product/delete',
            'controller' => 'ProductController',
            'action' => 'deleteProduct'
        ],
        'indexProducts' => [
            'method' => 'GET',
            'route' => '/',
            'controller' => 'IndexController',
            'action' => 'index'
        ],
        'addProduct' => [
            'method' => 'GET',
            'route' => '/add-product',
            'controller' => 'IndexController',
            'action' => 'create'
        ]
    ];    

    /**
     * Set the private attribute $routes in Src\Routes\Bootstrap
     */
    public function __construct()
    {
        $this->__set('routes', $this->routes);
    }
}
