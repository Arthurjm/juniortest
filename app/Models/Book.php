<?php

namespace App\Models;

class Book extends AbstractProductModel
{
    /**
     * Table mapped by Book's model
     *
     * @var string
     */
    protected $table = 'books';     
}