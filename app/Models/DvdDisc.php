<?php

namespace App\Models;

class DvdDisc extends AbstractProductModel
{
    /**
     * Table mapped by DvdDics's model
     *
     * @var string
     */
    protected $table = 'dvd_discs';        
}