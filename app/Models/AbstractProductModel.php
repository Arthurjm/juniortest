<?php

namespace App\Models;

use Src\Models\Model;

abstract class AbstractProductModel extends Model
{
    //Abstract class in case of any new exclusively product related method need to be created
}
