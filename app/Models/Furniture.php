<?php

namespace App\Models;

class Furniture extends AbstractProductModel
{
    /**
     * Table mapped by Furniture's model
     *
     * @var string
     */
    protected $table = 'furnitures';        
}