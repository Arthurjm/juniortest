<?php

namespace App\Models;

class Product extends AbstractProductModel
{
    /**
     * Table mapped by Product's model
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Create an attribute in this model containing it's child table columns
     *
     * @return \stdClass
     */
    public function type()
    {
        return $this->morphTo($this->type, $this->type_id);
    }  
}
