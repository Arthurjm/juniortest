<?php

require_once dirname(__DIR__).('/vendor/autoload.php');

//Get the DI Container from configuration file
$container = require_once dirname(__DIR__).('/config/container.php');

//Create the routes
$router = new App\Route();
$router($container);
$router->handle($_SERVER['REQUEST_URI']);