import Vue from 'vue'
import Vuex from 'vuex'
import Vuelidate from 'vuelidate'

const { default: axios } = require('axios');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.use(Vuex)
Vue.use(Vuelidate)

const store = new Vuex.Store({
    state:{
        item:{},
        massDelete:[],
        transaction:{ errors:[] }
    }
})

Vue.component('product-list-component', require('./components/ProductListComponent.vue').default)
Vue.component('navbar-component', require('./components/NavbarComponent.vue').default)
Vue.component('product-grid-component', require('./components/ProductGridComponent.vue').default)
Vue.component('product-card-component', require('./components/ProductCardComponent.vue').default)
Vue.component('footer-component', require('./components/FooterComponent.vue').default)
Vue.component('create-product-component', require('./components/CreateProductComponent.vue').default)
Vue.component('product-form-component', require('./components/ProductFormComponent.vue').default)
Vue.component('input-group-component', require('./components/InputGroupComponent.vue').default)
Vue.component('wrong-method-component', require('./components/WrongMethodComponent.vue').default)

const app = new Vue({
    el: '#app',
    store,    
});