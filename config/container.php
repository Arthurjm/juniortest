<?php
/**
 * Script of possible container configurations
 */

//Instantiates the DI Container
$containerBuilder = new \DI\ContainerBuilder();

//Set the DI Container configurations
$containerBuilder->addDefinitions([    
    
]);

return $containerBuilder->build();