<?php
/**
 * Script of database configurations
 */

return [
    'mysql' => [
        'dbhost' => 'DBHOST',
        'dbname' => 'DBNAME',
        'dbuser' => 'DBUSER',
        'dbpass' => 'DBPASS'
    ]
];
