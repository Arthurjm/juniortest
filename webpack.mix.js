let mix = require('laravel-mix');

mix.js('resources/js/app.js', 'js').vue().css('resources/css/app.css', 'css').setPublicPath('public_html');