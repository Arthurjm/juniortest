<?php

namespace Src\Database;

class DB
{
    /**
     * The database PDO instace.
     *
     * @var \PDO    
     */
    private static $db;

    /**
     * Instantiates the PDO instace.
     *
     * @return \PDO
     */
    public static function getInstace()
    {
        $databaseParams = require __DIR__ .('/../../config/database.php'); 
        $credentials = $databaseParams['mysql'];        
        $dbhost = $credentials['dbhost'];        
        $dbname = $credentials['dbname'];
        $dbuser = $credentials['dbuser'];
        $dbpass = $credentials['dbpass'];
        
        $pdo = new \PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
        self::$db = $pdo;
        return self::$db;
    }

    /**
     * Execute a query on the database.
     *
     * @param string $query
     * @param array $bindings
     * @return array
     */
    public static function query(string $query, array $bindings = [])
    {
        $stmt = self::$db->prepare($query);
        foreach ($bindings as $key => $value) {
            $stmt->bindParam($key, $value);
        }
        $status = $stmt->execute();
        return [
            'statement' => $stmt,
            'status' => $status
        ];
    }
    
    /**
     * Executes a delete query on the database.
     *
     * @param integer $id
     * @param string $table
     * @return array $stmt
     */
    public static function delete(int $id, string $table)
    {
        $stmt = self::query("DELETE FROM $table WHERE id  = :id", ['id' => $id]);
        return $stmt;
    }

    /**
     * Executes a insertion query on the database.
     *
     * @param array $fields
     * @param string $table
     * @return array
     */
    public static function insert(array $fields, string $table)
    {
        $fieldNames = array_keys($fields);
        $fieldNames = implode(', ', $fieldNames);
        $values = "'" . implode("', '", $fields) . "'";
        $stmt = self::query("INSERT INTO $table ($fieldNames) values ($values)");
        return $stmt;
    }

    /**
     * Returns the last inserted id.
     *
     * @return string|false
     */
    public static function getLastIsertedId()
    {
        return self::$db->lastInsertId();
    }
}
