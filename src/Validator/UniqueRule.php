<?php

namespace Src\Validator;

use Rakit\Validation\Rule;
use Src\Database\DB;

class UniqueRule extends Rule
{
    /**
     * Message to be sent in occurrence of a validation error
     *
     * @var string
     */
    protected $message = ":attribute :value has been used";

    /**
     * Params to be filled when using this rule
     *
     * @var array
     */
    protected $fillableParams = ['table', 'column', 'except'];

    /**
     * Check if the rule has been complied with
     *
     * @param [type] $value
     * @return boolean
     */
    public function check($value): bool
    { 
        $this->requireParameters(['table', 'column']);
        
        $column = $this->parameter('column');
        $table = $this->parameter('table');
        $except = $this->parameter('except');

        if ($except AND $except == $value) {
            return true;
        }
       
        $stmt = DB::query("select count(*) as count from `{$table}` where `{$column}` = :value", [':value' => $value]);        
        $data = $stmt['statement']->fetch(\PDO::FETCH_ASSOC);

        return intval($data['count']) === 0;
    }
}