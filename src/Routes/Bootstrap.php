<?php

namespace Src\Routes;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\AcceptHeader;
use Src\Views\View;

abstract class Bootstrap
{
    /**
     * List of available routes
     *
     * @var array
     */
    private $routes;

    /**
     * Injection of the application container
     *
     * @param ContainerInterface $container
     * @return void
     */
    public function __invoke(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Handle all incoming request to it's proper controllers and actions
     *
     * @param string $url
     * @return void
     */
    public function handle(string $url)
    {
        $url = parse_url($url, PHP_URL_PATH);
        foreach ($this->routes as $route) {
            if ($route['route'] == $url) {
                //Define the controller that should be instanciated
                $class = 'App\\Controllers\\' . $route['controller'];

                //Define the action that should be called
                $action = $route['action'];

                $request = Request::createFromGlobals();
                $response = new Response();                

                //Set the response standard content type to json
                $response->headers->set('Content-Type', 'application/json');
                $response->headers->set('Access-Control-Allow-Origin', '*');

                //Tests if the request method is the same as the one specified in $routes
                if (!$request->isMethod($route['method'])) {
                    $acceptHeader = AcceptHeader::fromString($request->headers->get('Accept'));

                    //Tests if client accepts content in application/json
                    if ($acceptHeader->has('application/json')) {
                        $response->setContent(json_encode(['status' => 405, 'message' => 'Method not allowed']))
                            ->setStatusCode(Response::HTTP_METHOD_NOT_ALLOWED);
                        return $response->send();
                    }

                    View::setLayout('wrong_method_layout');
                    return View::view('error/wrong_method');
                }
                $controller = $this->container->get($class);
                $controller->$action($request, $response);
            }
        }
    }

    /**
     * Dynamically retrieve attributes on the routes bootstrap class
     *
     * @param string $attr
     * @return mixed
     */
    public function __get(string $attr)
    {
        return $this->$attr;
    }

    /**
     * Dynamically set attributes on the routes bootstrap class
     *
     * @param string $attr
     * @param mixed $value
     * @return \Src\Routes\Bootstrap
     */
    public function __set(string $attr, $value)
    {
        $this->$attr = $value;
        return $this;
    }
}
