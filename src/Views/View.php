<?php

namespace Src\Views;

class View
{
    /**
     * The basic layout name.
     *
     * @var string 
     */
    private static $view;

    /**
     * The view to be render.
     *
     * @var string
     */
    private static $layout = 'layout';

    /**
     * Defines the view and render the layout.
     *
     * @param string $view
     * @return void     
     */
    public static function view(string $view)
    {                
        self::$view = $view;
        return require_once __DIR__ . ('/../../resources/views/layout/' . self::$layout . '.php');
    }

     /**
     * Render the view
     *     
     * @return void
     */
    public static function content()
    {
        return require_once __DIR__ . ('/../../resources/views/' . self::$view . '.php');
    }

    /**
     * Dynamically set the layout for the view
     *
     * @param [type] $layout
     * @return void
     */
    public static function setLayout($layout){
        self::$layout = $layout;
    }
}
