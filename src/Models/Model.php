<?php

namespace Src\Models;

use Src\Database\DB;

abstract class Model
{
    /**
     * Starts a new PDO instace.
     */
    public function __construct()
    {
        DB::getInstace();
    }

    /**
     * Insert a new registry in this model's table.
     *
     * @param array $data
     * @return array
     */
    public function insert(array $data)
    {
        $fields = implode(', ', array_keys($data));
        $values = "'" . implode("', '", $data) . "'";
        $stmt = DB::query("INSERT INTO $this->table ($fields) VALUES ($values)");
        return $stmt;
    }

    /**
     * Get all the records in this model's table
     *
     * @param array $columns
     * @return \Src\Models\Model
     */
    public function all(array $columns = ['*'])
    {
        $columns = implode(', ', $columns);
        $class = get_class($this);
        $stmt = DB::query("SELECT $columns FROM $this->table");
        $stmt['statement']->setFetchMode(\PDO::FETCH_CLASS, $class);
        return $stmt['statement']->fetchAll();
    }

    /**
     * Find one registry in this model's table by the id
     *
     * @param integer $id
     * @param array $columns
     * @return \Src\Models\Model
     */
    public function find(int $id, array $columns = ['*'])
    {
        $columns = implode(', ', $columns);
        $stmt = DB::query("SELECT $columns FROM $this->table WHERE id = :id", [':id' => $id]);
        $stmt['statement']->setFetchMode(\PDO::FETCH_INTO, $this);
        $stmt['statement']->fetch();
        return $this;
    }

    /**
     * Create an attribute in this model containing it's child table columns
     *
     * @param string $foreign_table
     * @param string $foreign_key
     * @return \stdClass
     */
    protected function morphTo(string $foreign_table, string $foreign_key)
    {
        $stmt = DB::query("SELECT * FROM $foreign_table WHERE $foreign_table.id = $foreign_key");
        $stmt['statement']->setFetchMode(\PDO::FETCH_OBJ);
        $result = $stmt['statement']->fetchAll();
        $this->info = $result;
        return $result;
    }

    /**
     * Delete from the database the registry containing, as it's id, this model's id
     *
     * @return \Src\Models\Model
     */
    public function destroy()
    {
        $stmt = DB::query("DELETE FROM $this->table WHERE id = $this->id");
        return $this;
    }

    /**
     * Dynamically retrieve attributes on the model
     *
     * @param string $attr
     * @return void
     */
    public function __get(string $attr)
    {
        return $this->$attr;
    }

    /**
     * Dynamically set attributes on the model
     *
     * @param string $attr
     * @param mixed $value
     * @return \Src\Models\Model
     */
    public function __set(string $attr, $value)
    {
        $this->$attr = $value;
        return $this;
    }
}
