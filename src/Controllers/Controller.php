<?php

namespace Src\Controllers;

use Src\Views\View;

abstract class Controller
{   
    /**
     * Calls the class View to render the view received as parameter
     *
     * @param string $view
     * @return void     
     */
    protected function view(string $view)
    {
       return View::view($view);
    }
}
